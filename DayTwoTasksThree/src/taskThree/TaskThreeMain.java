package taskThree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TaskThreeMain {

	public static void main(String[] args) throws IOException {
		
		File file = new File("resources/TextFile");
		
		BufferedReader bReader = new BufferedReader(new FileReader(file));
		
		String text = null;
		String line=bReader.readLine();
		
		while(line!=null) {
		text = text + line;
		line = bReader.readLine();
		}
		
		bReader.close();
		
		text = text.trim().replaceAll("[^\\w\\s]", "").toLowerCase();

		String[] textArr = text.split(" ");
		List<String> arrayList = Arrays.asList(textArr);

		TreeMap<String, Integer> theMap = new TreeMap<>();

		for (String string : arrayList) {

			int count = 0;
			for (int i = 0; i < arrayList.size(); i++) {
				if (string.equals(arrayList.get(i)))
					count++;
			}

			theMap.put(string, count);
		}

		for (Map.Entry<String, Integer> e : theMap.entrySet()) {
			System.out.println(e.getKey() + " " + e.getValue());
		}

	}
}
