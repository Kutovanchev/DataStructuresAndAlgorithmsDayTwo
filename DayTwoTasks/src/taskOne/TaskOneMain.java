package taskOne;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
public class TaskOneMain {
	
	public static void fillMap(Map<Integer, Integer> map) {
		Random random = new Random();
		while(map.size()<1000000) {
		map.put(random.nextInt(2000001),random.nextInt(2000001));
		}
	}
		
	
	public static void main(String[] args) {
		
	Map<Integer, Integer> HashMap = new HashMap<>();
	Map<Integer, Integer> TreeMap = new TreeMap<>();
	
	ArrayList<Long> times = new ArrayList<>();
	
	long start = System.nanoTime();
	fillMap(HashMap);
	long stop = System.nanoTime();
	
	times.add(stop-start);
	
	start = System.nanoTime();
	fillMap(TreeMap);
	stop = System.nanoTime();
	
	times.add(stop-start);
	
	
	System.out.println("Time to fill a Hash: " + times.get(0));
	System.out.println("Time to fill a Tree: " + times.get(1));
		
	}
}
