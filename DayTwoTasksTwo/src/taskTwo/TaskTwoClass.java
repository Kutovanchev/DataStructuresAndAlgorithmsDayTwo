package taskTwo;

public class TaskTwoClass implements Comparable<TaskTwoClass> {
	private int fieldOne;
	private int fieldTwo;
	private int fieldThree;

	public int getFieldOne() {
		return fieldOne;
	}

	public TaskTwoClass(int fieldOne, int fieldTwo, int fieldThree) {
		super();
		this.fieldOne = fieldOne;
		this.fieldTwo = fieldTwo;
		this.fieldThree = fieldThree;
	}

	@Override
	public int compareTo(TaskTwoClass o) {
		if (this.fieldOne != o.fieldOne || this.fieldTwo != o.fieldTwo || this.fieldThree != o.fieldThree) {
			return -1;
		}
		
		return 0;

		// return Integer.compare(this.fieldOne, o.fieldOne);
	}

}
