package taskTwo;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskTwoMain {

	public static void fillTreeMap(Map<TaskTwoClass, Integer> map) {
		Random random = new Random();
		int count = 0;
		while (count < 1000000) {
			map.put(new TaskTwoClass(random.nextInt(Integer.MAX_VALUE), random.nextInt(Integer.MAX_VALUE),
					random.nextInt(Integer.MAX_VALUE)), random.nextInt(2000001));
			count++;
		}
	}

	public static ArrayList<Integer> ReadEven(Map<TaskTwoClass, Integer> map) {

		ArrayList<Integer> numbers = new ArrayList<>();

		for (Entry<TaskTwoClass, Integer> entry : map.entrySet()) {

			if (entry.getValue() % 2 == 0) {
				numbers.add(entry.getValue());
			}
		}

		return numbers;
	}

	public static ArrayList<Integer> ReadUnEven(Map<TaskTwoClass, Integer> map) {

		ArrayList<Integer> unevenNumbers = new ArrayList<>();

		for (Entry<TaskTwoClass, Integer> entry : map.entrySet()) {

			if ((entry.getKey().getFieldOne() % 2) != 0) {
				unevenNumbers.add(entry.getKey().getFieldOne());
			}
		}

		return unevenNumbers;
	}

	public static void main(String[] args) throws InterruptedException {

		ConcurrentSkipListMap<TaskTwoClass, Integer> cSLM = new ConcurrentSkipListMap<>();
		// TreeMap<TaskTwoClass, Integer> cSLM = new TreeMap<>();

		ExecutorService service = Executors.newFixedThreadPool(3);

		Runnable f = () -> fillTreeMap(cSLM);
		service.submit(f);

		while (cSLM.size() < 1000000) {
			Thread.sleep(20);
			System.out.println(cSLM.size());
		}

		Runnable r = () -> ReadEven(cSLM);
		service.submit(r);

		Runnable u = () -> ReadUnEven(cSLM);
		service.submit(u);
		
		service.shutdown();

		ArrayList<Integer> evenNumbersInValues = ReadEven(cSLM);
		ArrayList<Integer> unevenNumberInKeys = ReadUnEven(cSLM);

			for (Integer integer : evenNumbersInValues) {
				System.out.println("Even number in values: " + integer);
			}

			for (Integer integer : unevenNumberInKeys) {
				System.out.println("Uneven number in keys: " + integer);
			}
			
		

	}
}
